// The messaging subsystem is used to publish messages from things to all subscribers

package messaging

import (
	hub2 "github.com/leandro-lugaresi/hub"
)

// Subscriber defines the function which will be executed when a message of a subscribed topic is available
type Subscriber func(subscription hub2.Subscription)

var bus *hub2.Hub

// Initialize the messaging subsystem
func InitMessaging() {
	bus = hub2.New()
}

// Publish the given data to the given topic. This also includes the timestamp (Unix nanoseconds) when the message occurred
func Publish(topic string, time int64, data string) {
	bus.Publish(hub2.Message{
		Name:   topic,
		Fields: hub2.Fields{"time": time, "message": data},
	})
}

// Subscribe to the given list of topics
func Subscribe(subscriber Subscriber, topics ...string) {
	sub := bus.NonBlockingSubscribe(10, topics...)
	go subscriber(sub)
}

func Close() {
	bus.Close()
}
