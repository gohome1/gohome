package messaging

import (
	"github.com/leandro-lugaresi/hub"
	"github.com/stretchr/testify/require"
	"sync"
	"testing"
	"time"
)

func TestPubSub(t *testing.T) {
	var wg sync.WaitGroup
	InitMessaging()

	wg.Add(1)
	Subscribe(func(s hub.Subscription) {
		for msg := range s.Receiver {
			require.Equal(t, int64(1), msg.Fields["time"])
			require.Equal(t, `{"temperature": 22}`, msg.Fields["message"])
		}
		wg.Done()
	}, "foo.bar")

	Publish("foo.bar", 1, `{"temperature": 22}`)

	Close()
	require.False(t, waitTimeout(&wg, time.Second*5))
}

// waitTimeout waits for the waitgroup for the specified max timeout.
// Returns true if waiting timed out.
func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wg.Wait()
	}()
	select {
	case <-c:
		return false // completed normally
	case <-time.After(timeout):
		return true // timed out
	}
}
