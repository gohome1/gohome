module gohome

go 1.12

require (
	github.com/dgraph-io/badger/v2 v2.0.1
	github.com/ilyakaznacheev/cleanenv v1.2.0
	github.com/leandro-lugaresi/hub v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
)
