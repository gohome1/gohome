// Implementation of a storage provider using badger key value database

package storage

import (
	"github.com/dgraph-io/badger/v2"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
)

type BadgerStorage struct {
	db *badger.DB
}

const KeyTimeDelimiter = ":"

// Open a database in the given file location
func (s *BadgerStorage) Open(file string) error {
	log.WithField("file", file).Debug("Opening database")

	opts := badger.DefaultOptions(file)
	opts.Logger = &defaultLog{}
	db, err := badger.Open(opts)
	s.db = db
	return err
}

// Close the previously opened database
func (s *BadgerStorage) Close() error {
	log.Debug("Closing database")
	return s.db.Close()
}

// Write data to the badger database
func (s *BadgerStorage) Write(key string, time int64, value string) error {
	err := s.db.Update(func(tx *badger.Txn) error {
		key = key + KeyTimeDelimiter + strconv.FormatInt(time, 10)

		log.WithFields(log.Fields{
			"key":   key,
			"value": value,
		}).Debug("Writing data")

		err := tx.Set([]byte(key), []byte(value))
		return err
	})

	return err
}

// Query data from the badger database. To disable offset and limit set them to 0
func (s *BadgerStorage) Query(key string, offset int, limit int) ([]*KeyValue, error) {
	result := make([]*KeyValue, 0)

	log.WithFields(log.Fields{
		"key":    key,
		"offset": offset,
		"limit":  limit,
	}).Debug("Query data")

	err := s.db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()
		prefix := []byte(key)

		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			if offset > 0 {
				offset--
				continue
			}

			item := it.Item()
			k := string(item.Key()[:])

			err := item.Value(func(v []byte) error {
				nanoTime := strings.TrimPrefix(k, key+KeyTimeDelimiter)
				result = append(result, &KeyValue{Key: nanoTime, Value: string(v[:])})
				return nil
			})

			if err != nil {
				return err
			}

			if limit > 0 {
				limit--
				if limit == 0 {
					break
				}
			}
		}
		return nil
	})

	return result, err
}

// Implementation of the logging system for badger
type defaultLog struct {
}

func (l *defaultLog) Errorf(f string, v ...interface{}) {
	log.Errorf(f, v...)
}

func (l *defaultLog) Warningf(f string, v ...interface{}) {
	log.Warnf(f, v...)
}

func (l *defaultLog) Infof(f string, v ...interface{}) {
	log.Infof(f, v...)
}

func (l *defaultLog) Debugf(f string, v ...interface{}) {
	log.Debugf(f, v...)
}
