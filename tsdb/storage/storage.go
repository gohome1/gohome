// Interface for different storage providers

package storage

type KeyValue struct {
	Key   string
	Value string
}

// Common interface for all storage implementations
type Storage interface {
	Open(file string) error
	Close() error
	Write(key string, time int64, value string) error
	Query(key string, offset int, limit int) ([]*KeyValue, error)
}

// Get a new storage engine powered by badger key value store
func NewBadgerStorage(file string) (Storage, error) {
	s := &BadgerStorage{}
	err := s.Open(file)
	return s, err
}
