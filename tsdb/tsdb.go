// Lightweight time series database implementation

package tsdb

import (
	"errors"
	"github.com/leandro-lugaresi/hub"
	log "github.com/sirupsen/logrus"
	"gohome/messaging"
	"gohome/tsdb/storage"
	"strconv"
)

type Tsdb struct {
	db storage.Storage
}

type TsdbConfig struct {
	Path          string   `yaml:"path" env-description:"Path to the database file" env-default:"tsdb.db"`
	Subscriptions []string `yaml:"subscriptions" env-description:"List of topics which should be persisted in the database. * (star) can substitute for exactly one word"`
}

type Data struct {
	Time  int64
	Value string
}

// Initialize the time series database with the given configuration
func InitTsdb(cfg *TsdbConfig) {
	db, err := newTsdb(cfg.Path)

	if err != nil {
		log.WithField("path", cfg.Path).Error("Unable to open database file. No data will be stored")
	}

	messaging.Subscribe(func(s hub.Subscription) {
		for msg := range s.Receiver {
			db.write(msg.Name, Data{
				Time:  msg.Fields["time"].(int64),
				Value: msg.Fields["message"].(string),
			})
		}
	}, cfg.Subscriptions...)
}

// NewTsdb returns a new time series database which will write to the location given in the file parameter
func newTsdb(file string) (*Tsdb, error) {
	db, err := storage.NewBadgerStorage(file)

	if err != nil {
		return nil, err
	}

	return &Tsdb{db: db}, nil
}

// Write writes the given data to the database
func (t *Tsdb) write(series string, data Data) error {
	if series == "" {
		return errors.New("series must not be empty")
	}

	if data.Time <= 0 {
		return errors.New("time must be a positive number")
	}

	if data.Value == "" {
		return errors.New("value must not be empty")
	}

	return t.db.Write(series, data.Time, data.Value)
}

// Query loads the data from the database sorted ascending by the timestamp
func (t *Tsdb) Query(series string, offset int, limit int) ([]*Data, error) {
	if series == "" {
		return nil, errors.New("series must not be empty")
	}

	result, err := t.db.Query(series, offset, limit)

	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return make([]*Data, 0), nil
	}

	var data []*Data
	for _, r := range result {
		time, _ := strconv.ParseInt(r.Key, 10, 64)
		data = append(data, &Data{Time: time, Value: r.Value})
	}

	return data, nil
}

// Close the database connection
func (t *Tsdb) close() error {
	return t.db.Close()
}
