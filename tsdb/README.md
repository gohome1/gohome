# Lightweight Time Series Database

TSDB is an embeddable lightweight time series database using [Badger](https://github.com/dgraph-io/badger) as backend

## Usage

### Opening a database

```go
package main

import (
    "log"
    "gtlab.com/gohome1/tsdb"
)

func main() {
    tsdb, err := NewTsdb("test.db")
    if err != nil {
        log.Fatal(err)
    }
    defer tsdb.Close()
    // Your code here…
}
```

### Writing data

```go
package main

func main() {
    // open database first
    data := Data{time: time.Now().UnixNano(), value: `{"temperature": 22}`}
    err := tsdb.Write("kitchen/temperature", data)

    if err != nil {
        log.Print("Write was not successful")
    }
}
```

### Reading data

```go
package main

func main() {
    // open database first
    // write some data
    result, err := tsdb.Query("kitchen/temperature", 0, 0)
}
```

Parameters:
* Series: The series to return
* Offset: Skips the first n items. 0 is default
* Limit: Get all the items until the limit of items is reached. 0 is default and will return all available items