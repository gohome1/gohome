package tsdb

import (
	"github.com/stretchr/testify/require"
	"os"
	"testing"
	"time"
)

func testOpen(t testing.TB) *Tsdb {
	if err := os.RemoveAll("test.db"); err != nil {
		t.Fatal(err)
	}

	tsdb, err := newTsdb("test.db")

	if err != nil {
		t.Fatal(err)
	}

	return tsdb
}

func testClose(tsdb *Tsdb) {
	tsdb.close()
	os.RemoveAll("test.db")
}

func TestWriteSuccess(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	data := Data{Time: time.Now().UnixNano(), Value: `{"temperature": 22}`}
	err := tsdb.write("kitchen.temperature", data)
	require.Nil(t, err)
}

func TestWriteEmptySeries(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	data := Data{Time: time.Now().UnixNano(), Value: `{"temperature": 22}`}
	err := tsdb.write("", data)
	require.NotNil(t, err)
}

func TestWriteNegativeTime(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	data := Data{Time: -1, Value: `{"temperature": 22}`}
	err := tsdb.write("kitchen.temperature", data)
	require.NotNil(t, err)
}

func TestWriteEmptyData(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	data := Data{Time: time.Now().UnixNano(), Value: ""}
	err := tsdb.write("kitchen.temperature", data)
	require.NotNil(t, err)
}

func TestRead(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736152000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736153000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736154000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736155000, Value: `{"temperature": 22}`})

	result, err := tsdb.Query("kitchen.temperature", 0, 0)

	require.NotNil(t, result)
	require.Nil(t, err)

	require.Len(t, result, 5)
}

func TestReadOrder(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736152000, Value: `{"temperature": 23}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736153000, Value: `{"temperature": 24}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736154000, Value: `{"temperature": 25}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736155000, Value: `{"temperature": 26}`})

	result, err := tsdb.Query("kitchen.temperature", 0, 0)

	require.NotNil(t, result)
	require.Nil(t, err)

	require.Equal(t, int64(1578765675736151000), result[0].Time)
	require.Equal(t, `{"temperature": 22}`, result[0].Value)
	require.Equal(t, int64(1578765675736152000), result[1].Time)
	require.Equal(t, `{"temperature": 23}`, result[1].Value)
	require.Equal(t, int64(1578765675736153000), result[2].Time)
	require.Equal(t, `{"temperature": 24}`, result[2].Value)
	require.Equal(t, int64(1578765675736154000), result[3].Time)
	require.Equal(t, `{"temperature": 25}`, result[3].Value)
	require.Equal(t, int64(1578765675736155000), result[4].Time)
	require.Equal(t, `{"temperature": 26}`, result[4].Value)
}

func TestReadOrderWithUnorderedInsert(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	tsdb.write("kitchen.temperature", Data{Time: 1578765675736155000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736154000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736152000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736153000, Value: `{"temperature": 22}`})

	result, err := tsdb.Query("kitchen.temperature", 0, 0)

	require.NotNil(t, result)
	require.Nil(t, err)

	require.Equal(t, int64(1578765675736151000), result[0].Time)
	require.Equal(t, int64(1578765675736152000), result[1].Time)
	require.Equal(t, int64(1578765675736153000), result[2].Time)
	require.Equal(t, int64(1578765675736154000), result[3].Time)
	require.Equal(t, int64(1578765675736155000), result[4].Time)
}

func TestReadOffset(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736152000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736153000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736154000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736155000, Value: `{"temperature": 22}`})

	result, err := tsdb.Query("kitchen.temperature", 2, 0)

	require.NotNil(t, result)
	require.Nil(t, err)
	require.Len(t, result, 3)

	require.Equal(t, int64(1578765675736153000), result[0].Time)
	require.Equal(t, int64(1578765675736154000), result[1].Time)
	require.Equal(t, int64(1578765675736155000), result[2].Time)
}

func TestReadLimit(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736152000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736153000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736154000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736155000, Value: `{"temperature": 22}`})

	result, err := tsdb.Query("kitchen.temperature", 0, 3)

	require.NotNil(t, result)
	require.Nil(t, err)

	require.Len(t, result, 3)

	require.Equal(t, int64(1578765675736151000), result[0].Time)
	require.Equal(t, int64(1578765675736152000), result[1].Time)
	require.Equal(t, int64(1578765675736153000), result[2].Time)
}

func TestReadOffsetAndLimit(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736152000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736153000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736154000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736155000, Value: `{"temperature": 22}`})

	result, err := tsdb.Query("kitchen.temperature", 1, 3)

	require.NotNil(t, result)
	require.Nil(t, err)

	require.Len(t, result, 3)

	require.Equal(t, int64(1578765675736152000), result[0].Time)
	require.Equal(t, int64(1578765675736153000), result[1].Time)
	require.Equal(t, int64(1578765675736154000), result[2].Time)
}

func TestReadNoData(t *testing.T) {
	tsdb := testOpen(t)
	defer testClose(tsdb)

	tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
	tsdb.write("kitchen.temperature", Data{Time: 1578765675736152000, Value: `{"temperature": 22}`})

	result, err := tsdb.Query("livingroom/temperature", 0, 0)

	require.NotNil(t, result)
	require.Nil(t, err)

	require.Len(t, result, 0)
}

func BenchmarkWrite(b *testing.B) {
	tsdb := testOpen(b)
	defer testClose(tsdb)

	b.Run("TSDB Write Benchmark", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			tsdb.write("kitchen.temperature", Data{Time: 1578765675736151000, Value: `{"temperature": 22}`})
		}
	})
}
