package main

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"gohome/logging"
	"gohome/messaging"
	"gohome/tsdb"
	"os"
)

type Config struct {
	LogCfg  logging.LogConfig `yaml:"logging" env-description:"Configure the logging subsystem"`
	TsdbCfg tsdb.TsdbConfig   `yaml:"tsdb" env-description:"Configure the time series database subsystem"`
}

func main() {
	var cfg Config

	// read configuration from the file and environment variables
	if err := cleanenv.ReadConfig("/home/msi/Projects/gohome/cmd/gohome/config.yml", &cfg); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	// initialize the application
	logging.InitLogManager(&cfg.LogCfg)
	messaging.InitMessaging()
	tsdb.InitTsdb(&cfg.TsdbCfg)
}
