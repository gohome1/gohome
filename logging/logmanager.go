// Manage and configure the logging subsystem

package logging

import (
	log "github.com/sirupsen/logrus"
	"strings"
)

type LogConfig struct {
	Level string `yaml:"level" env-description:"Log level (trace, debug, info, warn, error, fatal, panic)" env-default:"info"`
}

// InitLogManger initializes the logging system with the given configuration
func InitLogManager(cfg *LogConfig) {
	log.SetLevel(getLogLevelByString(cfg.Level))
}

// Get the correct log level enum from the given string. Unknown log levels will default to 'info'
func getLogLevelByString(level string) log.Level {
	switch strings.ToLower(level) {
	case "trace":
		return log.TraceLevel
	case "debug":
		return log.DebugLevel
	case "info":
		return log.InfoLevel
	case "warn":
		return log.WarnLevel
	case "error":
		return log.ErrorLevel
	case "fatal":
		return log.FatalLevel
	case "panic":
		return log.PanicLevel
	}

	return log.InfoLevel
}
